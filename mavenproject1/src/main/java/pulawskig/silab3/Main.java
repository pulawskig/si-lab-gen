package pulawskig.silab3;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static org.jenetics.engine.EvolutionResult.toBestPhenotype;
import static org.jenetics.engine.limit.bySteadyFitness;

import java.util.Arrays;
import org.jenetics.DoubleGene;
import org.jenetics.Mutator;
import org.jenetics.Optimize;
import org.jenetics.Phenotype;
import org.jenetics.RouletteWheelSelector;
import org.jenetics.SinglePointCrossover;
import org.jenetics.engine.Engine;
import org.jenetics.engine.EvolutionStatistics;
import org.jenetics.engine.codecs;
import org.jenetics.util.DoubleRange;

public class Main {
    // The fitness function.

    public static final int N = 20;
    public static final int A = 10;
    
    private static double fitness(final double[] x) {
        return A * N + Arrays.stream(x).map( (y) -> (y * y - A * cos(2 * PI * y)) ).sum();
        
    }

    public static void main(final String[] args) {
        final Engine<DoubleGene, Double> engine = Engine
                // Create a new builder with the given fitness
                // function and chromosome.
                .builder(
                        Main::fitness,
                        codecs.ofVector(DoubleRange.of(-10d, 10d), N))
                .populationSize(5000)
                .optimize(Optimize.MINIMUM)
                .alterers(
                        new Mutator<>(0.03),
                        new SinglePointCrossover<>(0.8))
                // Selektor ruletki
                .selector(new RouletteWheelSelector<>())
                // Build an evolution engine with the
                // defined parameters.
                .build();
        // Create evolution statistics consumer.
        final EvolutionStatistics<Double, ?> statistics = EvolutionStatistics.ofNumber();
        final Phenotype<DoubleGene, Double> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(100))
                // The evolution will stop after maximal 100
                // generations.
                .limit(100000)
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());
        System.out.println(statistics);
        System.out.println(best);
    }
}
